package com.wk;

import com.jcraft.jsch.*;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Vector;

public class JschClientTool {
    public static void main(String[] arg) {
/*
        FtpClientServiceImpl ftpClientService = new FtpClientServiceImpl();
        FTPFile[] files = ftpClientService.listFiles("./ct-data/plc/workflow/");
        for (FTPFile ftpFile : files) {
            System.out.println(ftpFile.getName());
        }
*/

        JSch jsch = new JSch();
        Session session = null;
        try {
            //
            session = jsch.getSession("asadmin", ".int.westgroup.com", 22);
            //session.setConfig("PreferredAuthentications", "password");
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.setPassword("");
            session.connect(1000);
            Channel channel = session.openChannel("sftp");
            ChannelSftp sftp = (ChannelSftp) channel;
            sftp.connect(1000);

            sftp.cd("/ct-data/plc/workflow/");
            Vector filelist = sftp.ls("/ct-data/plc/workflow/");
            for(int i=0; i<filelist.size();i++){
                System.out.println(filelist.get(i).toString());
            }
            InputStream inputStream = sftp.get("/ct-data/plc/workflow/test.txt");

            String text = IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());
            System.out.println(text);
            File f1 = File.createTempFile("test", "txt");
            sftp.put(new FileInputStream(f1), f1.getName());//test8162407445458499464txt
            sftp.exit();
            channel.disconnect();
            session.disconnect();

        } catch (JSchException | SftpException | IOException e) {
            e.printStackTrace();
        } finally {
            session.disconnect();
        }
    }
}
